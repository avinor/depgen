package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"text/template"
)

var tpl = flag.String("t", "deployment.tpl", "Deployment file template")
var prv = flag.String("p", "providers.json", "Providers data file in JSON format")

func main() {
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), `deployment.yml file generator utility.`)
		fmt.Fprintf(flag.CommandLine.Output(), "\nUsage:\n")
		flag.PrintDefaults()
		fmt.Fprint(flag.CommandLine.Output(), "\nExample:  depgen -t deployment.tpl -p providers.json > deployment.yml\n")
	}
	flag.Parse()

	var providers Providers
	providerSource, err := ioutil.ReadFile(*prv)
	if err != nil {
		log.Fatal(err)
	}
	err = json.Unmarshal(providerSource, &providers)
	if err != nil {
		log.Fatal(err)
	}

	tpl := template.Must(template.ParseFiles(*tpl))
	err = tpl.Execute(os.Stdout, map[string]interface{}{
		"Providers": providers.Split(),
	})
	if err != nil {
		log.Fatal(err)
	}
}

type Providers []*Provider

func (providers Providers) Split() Providers {
	var (
		defaults Provider
		others   = providers[0:0]
	)
	for _, p := range providers {
		if p.Name != "default" {
			others = append(others, p)
		} else {
			defaults = *p
		}
	}
	for _, p := range others {
		p.InitWithDefaults(defaults)
	}
	return others
}

type Provider struct {
	Code       string      `json:"code"`
	Name       string      `json:"name"`
	Download   *Download   `json:"download"`
	Downloads  []*Download `json:"downloads"`
	Copy       *Lambda     `json:"copy"`
	Categories *Lambda     `json:"categories"`
	Sync       *Lambda     `json:"sync"`
	Transform  *Lambda     `json:"transform"`
	Transforms []*Lambda   `json:"transforms"`
}

func (p Provider) Title() string {
	return strings.Title(p.Name)
}

func (p *Provider) InitWithDefaults(defaults Provider) {
	if p.Download != nil {
		p.Downloads = append(p.Downloads, p.Download)
	}
	for _, download := range p.Downloads {
		download.InitWithDefaults(defaults.Download)
	}
	p.Copy.InitWithDefaults(defaults.Copy)
	p.Categories.InitWithDefaults(defaults.Categories)
	p.Sync.InitWithDefaults(defaults.Sync)
	if p.Transform != nil {
		p.Transforms = append(p.Transforms, p.Transform)
	}
	for _, transform := range p.Transforms {
		transform.InitWithDefaults(defaults.Transform)
	}
}

type Download struct {
	Lambda
	URL      string `json:"url"`
	User     string `json:"user"`
	Password string `json:"password"`
}

func (d Download) Method() string {
	parse, err := url.Parse(d.URL)
	if err != nil {
		log.Fatal(err)
	}
	if strings.Contains(parse.Host, "yadi.sk") {
		return "yadisk"
	} else if strings.Contains(parse.Scheme, "http") {
		return "http"
	}
	return parse.Scheme
}

type Defaults interface {
	MemoryValue() int
	TimeoutValue() int
	BucketValue() string
}

type Lambda struct {
	Memory  int      `json:"memory"`
	Timeout int      `json:"timeout"`
	Bucket  string   `json:"bucket"`
	In      string   `json:"in"`
	Out     []string `json:"out"`
}

func (l Lambda) InFile() string {
	return l.In
}

func (l Lambda) OutFiles() string {
	return strings.Join(l.Out, string(filepath.ListSeparator))
}

func (l *Lambda) BucketValue() string {
	return l.Bucket
}

func (l *Lambda) MemoryValue() int {
	return l.Memory
}

func (l *Lambda) TimeoutValue() int {
	return l.Timeout
}

func (l *Lambda) InitWithDefaults(defaults Defaults) {
	if l == nil || reflect.ValueOf(defaults).IsNil() {
		return
	}
	if l.Bucket == "" {
		l.Bucket = defaults.BucketValue()
	}
	if l.Memory == 0 {
		l.Memory = defaults.MemoryValue()
	}
	if l.Timeout == 0 {
		l.Timeout = defaults.TimeoutValue()
	}
}
