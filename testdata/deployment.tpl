﻿{{range $provider := .Providers}}{{range $i, $download := $provider.Downloads}}{{$length := len $provider.Downloads}}
  Download{{$provider.Title}}{{if gt $length 1}}{{$i}}{{end}}:
    Type: AWS::Serverless::Function
    Properties:
      FunctionName: {{$provider.Code}}{{if gt $length 1}}{{$i}}{{end}}-download
      Runtime: go1.x
      Handler: main
      CodeUri: cmd/download/{{$download.Method}}/main.zip
      MemorySize: {{$download.Memory}}
      Timeout: {{$download.Timeout}}
      Policies:
        - AWSLambdaBasicExecutionRole
        - AmazonS3FullAccess
      Events:
        Download{{$provider.Title}}Event:
          Type: Schedule
          Properties:
            Schedule: cron(0 0 * * ? *) # 0:00am (UTC) everyday
      Environment:
        Variables:
          DOWNLOAD_URL: {{$download.URL}}
          USERNAME: "{{$download.User}}"
          PASSWORD: "{{$download.Password}}"
          DST_BUCKET: !Ref {{$download.Bucket}}
          DST_FILENAMES: "{{$download.OutFiles}}"
  {{end}}{{if $provider.Copy}}
  Copy{{$provider.Title}}:
    Type: AWS::Serverless::Function
    Properties:
      FunctionName: {{$provider.Code}}-copy
      Runtime: go1.x
      Handler: main
      CodeUri: cmd/copy/main.zip
      MemorySize: {{$provider.Copy.Memory}}
      Timeout: {{$provider.Copy.Timeout}}
      Policies:
        - AWSLambdaBasicExecutionRole
        - AmazonS3FullAccess
      Events:
        Copy{{$provider.Title}}Event:
          Type: S3
          Properties:
            Bucket: !Ref SourceBucket
            Events:
              - s3:ObjectCreated:*
            Filter:
              S3Key:
                Rules:
                  - Name: suffix
                    Value: {{$provider.Copy.InFile}}
      Environment:
        Variables:
          SRC_BUCKET: !Ref SourceBucketName
          DST_BUCKET: !Ref SourceBucketName
          DST_FILENAMES: "{{$provider.Copy.OutFiles}}"
  {{end}}{{if $provider.Categories}}
  Categories{{$provider.Title}}:
    Type: AWS::Serverless::Function
    Properties:
      FunctionName: {{$provider.Code}}-extract
      Runtime: go1.x
      Handler: main
      CodeUri: cmd/{{$provider.Code}}/extractor/main.zip
      MemorySize: {{$provider.Categories.Memory}}
      Timeout: {{$provider.Categories.Timeout}}
      Policies:
        - AWSLambdaBasicExecutionRole
        - AmazonS3FullAccess
      Events:
        Categories{{$provider.Title}}Event:
          Type: S3
          Properties:
            Bucket: !Ref SourceBucket
            Events:
              - s3:ObjectCreated:*
            Filter:
              S3Key:
                Rules:
                  - Name: suffix
                    Value: {{$provider.Categories.InFile}}
      Environment:
        Variables:
          SRC_BUCKET: !Ref SourceBucketName
          DST_BUCKET: !Ref SourceBucketName
          DST_FILENAME: "{{$provider.Categories.OutFiles}}"
  {{end}}{{if $provider.Sync}}
  Sync{{$provider.Title}}:
    Type: AWS::Serverless::Function
    Properties:
      FunctionName: {{$provider.Code}}-categories
      Runtime: go1.x
      Handler: main
      CodeUri: cmd/sync/category/main.zip
      MemorySize: {{$provider.Sync.Memory}}
      Timeout: {{$provider.Sync.Timeout}}
      Policies:
        - AWSLambdaBasicExecutionRole
        - AmazonS3FullAccess
      Events:
        Sync{{$provider.Title}}Event:
          Type: S3
          Properties:
            Bucket: !Ref SourceBucket
            Events:
              - s3:ObjectCreated:*
            Filter:
              S3Key:
                Rules:
                  - Name: suffix
                    Value: {{$provider.Sync.InFile}}
      Environment:
        Variables:
          DB_USER: !Ref DbUser
          DB_PASSWORD: !Ref DbPassword
          DB_HOST: !Ref DbHost
          DB_PORT: !Ref DbPort
          DB_NAME: !Ref DbName
  {{end}}{{range $i, $transform := $provider.Transforms}}{{$length := len $provider.Transforms}}
  Transform{{$provider.Title}}{{if gt $length 1}}{{$i}}{{end}}:
    Type: AWS::Serverless::Function
    Properties:
      FunctionName: {{$provider.Code}}{{if gt $length 1}}{{$i}}{{end}}-transform
      Runtime: go1.x
      Handler: main
      CodeUri: cmd/{{$provider.Code}}/transform/main.zip
      MemorySize: {{$transform.Memory}}
      Timeout: {{$transform.Timeout}}
      Policies:
        - AWSLambdaBasicExecutionRole
        - AmazonS3FullAccess
      Events:
        Transform{{$provider.Title}}Event:
          Type: S3
          Properties:
            Bucket: !Ref SourceBucket
            Events:
              - s3:ObjectCreated:*
            Filter:
              S3Key:
                Rules:
                  - Name: suffix
                    Value: {{$transform.InFile}}
      Environment:
        Variables:
          SRC_BUCKET: !Ref SourceBucketName
          DST_BUCKET: !Ref ResultBucketName
          DST_FILENAME: {{$transform.OutFiles}}
          API_URL: !Sub "https://${ServerlessRestApi}.execute-api.${AWS::Region}.amazonaws.com/Prod/categories?provider={{$provider.Name}}"
          API_USERNAME: !Ref ApiUser
          API_PASSWORD: !Ref ApiPassword
  {{end}}
{{end}}
